import csv
import yaml
import sys
import logging


logger = logging.getLogger(__name__)


class TemplateSender:

    def __init__(self, **config_pars):
        self.from_ = config_pars['from']
        self.smtp_server = config_pars['smtp_server']
        self.port = config_pars['port']
        self.sender = config_pars['sender']
        self.password = config_pars['password']
        self.server = smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context)

    def open(self):
        self.server.login(self.sender, self.password)

    def close(self):
        pass

    def __exit__(self):
        pass

    def set_template(self, template_name):
        self.template = None

    def _fill_data(self, html_content, addressee):
        pass

    def _process_attachments(self, html_content, message):
        pass

    def _process_images(self, html_content, message):
        pass

    def send(self, addressee_data):
        html_content = self._template['html_content']



if __name__ == '__main__':
    sender = TemplateSender(config)
    template_name = sys.argv[1]
    sender.set_template(template_name)

    outobox_fn = sys.argv[2]
    addressees = []
    with open(outbox_fn, 'r') as outbox:
        for addressee in csv.DictReader(outbox):
            addressees.append(addresee)

    sent_fn = sys.argv[3] if len(sys.argv) > 3 else 'sent.csv'
    sent_counter  = 0

    with open(outbox_fn, 'w') as outbox:
        with open(sent_fn, 'a') as sent:
            box_writer =  csv.DictWriter(outbox)
            sent_writer = csv.DictWriter(sent)
            for addressee in addressees:
                try:
                    sender.send(addressee)
                    sent_writer.writerow(addressee)
                    sent_counter += 1
                except Exception as e:
                    logger.exception(e)
                    box_writer(addressee)

    print('Sent {} mails, see {} for checking addressee'.format(sent_counter, sent_fn))
